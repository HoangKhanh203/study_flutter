class Question {
  final int id, answer;
  final String question;
  final List<String> options;

  Question({this.id, this.question, this.answer, this.options});
}

const List sample_data = [
  {
    "id": 1,
    "question": "Địa chỉ Huflit",
    "options": ['Q7', 'Q10', 'Q11', 'Q5'],
    "answer_index": 1,
  },
  {
    "id": 2,
    "question": "Flutter ra mắt nằm nào.",
    "options": ['Jun 2017', 'March 2017', 'May 2017', 'May 2018'],
    "answer_index": 2,
  },
  {
    "id": 3,
    "question": "1 + 2 = ?.",
    "options": ['1', '2', '3', '4'],
    "answer_index": 2,
  },
  {
    "id": 4,
    "question": "2 + b = 4 => b =  ?",
    "options": ['0', '1', '2', '3'],
    "answer_index": 2,
  },
];
